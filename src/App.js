import React, { Component } from "react"
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import configureStore from './store.js'
import Home from './components/organisms/Home'
import Layout from './components/layout'

class App extends Component {

  render() {
    const store = configureStore();

    return ( 
      <Provider store={store}>
        <Router>
          <Layout>
            <Switch>
              <Route exact path="/" component={Home} />
            </Switch>
          </Layout>
        </Router>
      </Provider>
    )
  }
}

export default App
