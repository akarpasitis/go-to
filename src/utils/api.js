import fetch from 'node-fetch'
const API_KEY = 'Amadeus'
// MOVE KEY TO ENV

export default async (coords) => {
  const options = {
    method: 'GET',
  }
  const baseUrl = `https://api.sandbox.amadeus.com/v1.2/airports/nearest-relevant?apikey=${API_KEY}`
  const endpoint = `&latitude=${coords.lat}&longitude=${coords.lng}`

  const response = await fetch(baseUrl + endpoint, options);
  const body = await response.json();
  if (response.status !== 200) {
    throw Error(body.message)
  }
  return body;
}