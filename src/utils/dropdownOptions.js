const optionsPeople = [
  {
    id: 1,
    key: 1,
    text: 'Loner and Loving It (1)',
    value: 1
  },
  {
    id: 2,
    key: 2,
    text: 'Just Two... cough cough (2)',
    value: 2
  },
  {
    id: 3,
    key: 3,
    text: 'The Musketeers (3)',
    value: 3
  },
  {
    id: 4,
    key: 4,
    text: 'Getting Crowded (4)',
    value: 4
  },
  {
    id: 5,
    key: 5,
    text: 'No Pairs Events (5)',
    value: 5
  },
  {
    id: 6,
    key: 6,
    text: 'Too Many! (6)',
    value: 6
  }
]
const optionsBudget = [
  {
    id: 2000,
    key: 2000,
    text: 'Kind of Stingy (up to 2000 EUR)',
    value: 2000
  },
  {
    id: 4000,
    key: 4000,
    text: 'Saved Up (up to 4000 EUR)',
    value: 4000
  },
  {
    id: 6000,
    key: 6000,
    text: 'Got Some Spare Cash (up to 6000 EUR)',
    value: 6000
  },
  {
    id: 8000,
    key: 8000,
    text: 'I\'m Doing Well Bro! (up to 8000 EUR)',
    value: 8000
  },
  {
    id: 15000,
    key: 15000,
    text: 'Big Spender (up to 15000 EUR)',
    value: 15000
  }
]
const optionsDuration = [
  {
    id: 6,
    key: 6,
    text: 'Short and Sweet (up to 6 Hours each way)',
    value: 6
  },
  {
    id: 12,
    key: 12,
    text: 'Somewhere in the middle (up to 12 Hours each way)',
    value: 12
  },
  {
    id: 24,
    key: 24,
    text: 'Wanna Go the Distance (up to 24 Hours each way)',
    value: 24
  },
  {
    id: 48,
    key: 48,
    text: 'Got Lots of Time Baby (up to 48 Hours each way)',
    value: 48
  }
]

const options = { optionsPeople, optionsBudget, optionsDuration }

export default options