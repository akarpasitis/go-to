import { createStore, applyMiddleware, compose } from 'redux'
import reducers from './reducers'

const logger = store => next => action => {
  console.group(action.type)
  console.info('dispatching', action)
  let result = next(action)
  console.log('next state', store.getState().toJS())
  console.groupEnd(action.type)
  return result
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const middlewares = {
  logger
}

export default (initialState, ...middleWares) => {
  return createStore(
    reducers,
    initialState,
    composeEnhancers(applyMiddleware(...middleWares))
  )
}
