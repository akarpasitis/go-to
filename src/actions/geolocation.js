import Geocode from "react-geocode"
Geocode.setApiKey("google");
// MOVE KEY TO ENV

// Geocode.enableDebug();
// Enable for console logs

export const getCoords = (dispatch, city) => {
  dispatch({
    type: 'COORDS_PENDING'
  })
  Geocode.fromAddress(city).then(
    response => {
      const { lat, lng } = response.results[0].geometry.location;
      dispatch({
        type: 'COORDS_SET',
        coords: { lat, lng }
      })
    },
    error => {
      console.error(error);
    }
  )
}