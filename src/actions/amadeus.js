import apiCall from '../utils/api.js'
import destinations from '../utils/destinations.js'

export const filterDestinations = (dispatch, preference) => {
  let filteredDestinations = { 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: []}

  destinations.forEach(destination => {
    if (preference === destination.Type1.toLowerCase()) {
      filteredDestinations['1'].push(destination)
    }
    if (preference === destination.Type2.toLowerCase()) {
      filteredDestinations['2'].push(destination)
    }
    if (preference === destination.Type3.toLowerCase()) {
      filteredDestinations['3'].push(destination)
    }
    if (preference === destination.Type4.toLowerCase()) {
      filteredDestinations['4'].push(destination)
    }
    if (preference === destination.Type5.toLowerCase()) {
      filteredDestinations['5'].push(destination)
    }
    if (preference === destination.Type6.toLowerCase()) {
      filteredDestinations['6'].push(destination)
    }
    if (preference === destination.Type7.toLowerCase()) {
      filteredDestinations['7'].push(destination)
    }
  })
  console.warn(filteredDestinations)
  return filteredDestinations
}

export const getAirports = (dispatch, coords) => {
  dispatch({
    type: 'AIRPORTS_PENDING'
  }) 
  return apiCall(coords).then(res => {
      dispatch({
        type: 'AIRPORTS_SET',
        airports: res
      })
    }).catch(err => {
      console.log(err)
    });
}