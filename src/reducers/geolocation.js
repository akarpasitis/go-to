const INITIAL_STATE = {
  coords: {},
  coordsPending: false
}

const setCoords = (state, coords) => ({ ...state, coords, coordsPending: false })
const setCoordsPending = (state, coordsPending) => ({ ...state, coordsPending })

export default(state = INITIAL_STATE, action) => {
  switch(action.type) {
    case 'COORDS_SET':
      return setCoords(state, action.coords)
    case 'COORDS_PENDING':
      return setCoordsPending(state, true)
    default:
      return state
  }
}