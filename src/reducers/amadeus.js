const INITIAL_STATE = {
  airports: [],
  airportsPending: false
}

const setAirports = (state, airports) => ({ ...state, airports, airportsPending: false })
const setAirportsPending = (state, airportsPending) => ({ ...state, airportsPending })


export default(state = INITIAL_STATE, action) => {
  switch(action.type) {
    case 'AIRPORTS_SET':
      return setAirports(state, action.airports)
    case 'AIRPORTS_PENDING':
      return setAirportsPending(state, true)
    default:
      return state
  }
}