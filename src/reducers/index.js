import { combineReducers } from 'redux'
import amadeus from './amadeus'
import geolocation from './geolocation'

const rootReducer = combineReducers({
  amadeus,
  geolocation
})

export default rootReducer