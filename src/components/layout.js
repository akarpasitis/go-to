import React, { Component } from "react"
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import style from "./layout.scss"
import _ from 'lodash'

class Layout extends Component {
  render() {
    const marginLeft = !_.get(this.props, 'siderBarOn') ? '5vw' : 'calc(5vw + 290px)'
    const marginRight = !_.get(this.props, 'siderBarOn') ? '5vw' : '2vw'
    return ( 
      <div className={style.appContent}>
        <div
          className={style.content}
          style={{
            marginLeft: marginLeft,
            marginRight: marginRight
          }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    state
  }
}

export default withRouter(connect(mapStateToProps)(Layout))
