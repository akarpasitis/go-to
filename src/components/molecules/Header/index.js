import React, { Component } from "react"
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import style from "./index.scss"
import { Input, Menu, Icon } from 'semantic-ui-react'

class Header extends Component {
  state = {
    sideBarOn: false
  }
  handleItemClick = (e, { name }) =>  {
    this.setState({activeItem: name })
    if (name === 'login') {
      this.props.history.push("/login");
    }
    if (name === 'gear') {
      this.setState({ sideBarOn: true })
      sideBarActions.toggleSideBar(this.props.dispatch)
    }
  }


  render() {
    const { activeItem } = this.state

    return (
      <div>
        <Menu
          fixed="top"
          style={{ backgroundColor: "#1A1A1D", borderRadius: "0" }}
        >
          <Menu.Menu position="left">
            <Input
              focus
              inverted
              transparent
              style={{ color: 'white', paddingLeft: '20px' }}
              icon={{ name: "search", color: "grey"}}
              placeholder='Search Projects...'
            />
          </Menu.Menu>
          <Menu.Menu position="right">
            <Menu.Item
              name="login"
              active={activeItem === "login"}
              onClick={this.handleItemClick}
              style={{ color: "white" }}
            >
              Login/Signup
            </Menu.Item>
            <Menu.Item
              name="collaborate"
              active={activeItem === "collaborate"}
              onClick={this.handleItemClick}
              style={{ color: "white" }}
            >
              Collaborate
            </Menu.Item>
            <Menu.Item
              name="produce"
              active={activeItem === "produce"}
              onClick={this.handleItemClick}
              style={{ color: "white" }}
            >
              Produce
            </Menu.Item>{" "}
            <Menu.Item
              name="gear"
              active={activeItem === "gear"}
              onClick={this.handleItemClick}
              style={{ color: "#c3073f" }}
            >
              <Icon name='setting' size='big'/>
            </Menu.Item>{" "}
          </Menu.Menu>
        </Menu>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    state
  }
}

export default withRouter(connect(mapStateToProps)(Header))