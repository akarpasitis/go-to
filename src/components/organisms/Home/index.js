import React, { Component } from "react"
import style from "./index.scss"
import { connect } from 'react-redux'
import { Modal, Message, Divider, Button, Segment, Form, Input, Select, Checkbox } from 'semantic-ui-react'
import { RingLoader } from 'react-spinners'
import moment from 'moment'
import * as GeoActions from '../../../actions/geolocation'
import * as AmadeusActions from '../../../actions/amadeus'
import _ from 'lodash'
import options from '../../../utils/dropdownOptions'
import destinations from '../../../utils/destinations'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      city: 'Barcelona',
      checkDates: true,
      today: moment(new Date()).format("YYYY-MM-DD"),
      loading: false,
      people: 1,
      budget: 2000,
      duration: 6,
      startDate: moment(new Date()).add(1, 'months').format("YYYY-MM-DD"),
      stopDate: moment(new Date()).add(1, 'months').add(7, 'days').format("YYYY-MM-DD"),
      preference: 'adventure',
      showError: false,
      error: [
        {
          errorMessage: ''
        }
      ]
    }
  }

  componentWillReceiveProps(nextProps) {
    if (_.size(nextProps.coords) === 2 && !this.state.loading) {
      AmadeusActions.getAirports(this.props.dispatch, nextProps.coords)
      this.setState({ loading: true })
    }
  }

  handleInput = (e, type) => {
    this.setState({ [e.target.name]:  e.target.value })
  }

  handleDropdown = (e, { name, value }) => {
    this.setState({ [name]: value })
  }

  handleChange = (e, pref) => {
    this.setState({ preference: pref })
  }

  checkDates = type => {}

  submit = (e) => {
    e.preventDefault()

    // MOVE ALL THIS TO AMADEUS ACTION
    // PASS ALL DATA TO FUNCTION "analyzeFlights"
    this.setState({
      rankedDestinations: AmadeusActions.filterDestinations(
        this.props.dispatch, this.state.preference
      )
    })
    GeoActions.getCoords(this.props.dispatch, this.state.city)
  }

  render() {
    return (
      <div className={style.container}>
        <Modal size={'mini'} open={this.state.loading} className={style.modal}>
          <Modal.Header className={style.modalHeader}>
            Please wait while we are verifying your account...
          </Modal.Header>
          <Modal.Content className={style.spinner}>
            <RingLoader
              color={'#c3073f'}
              size={24}
              loading={true}
            />
          </Modal.Content>
        </Modal>
        <Segment compact size='small' className={style.segment}>
          <div className={style.login}>
            <h2 className={style.welcome}>
              How Are You Feeling?
            </h2>
            <h3>Fill in the (not so personal) questions below and will tell you goTo!</h3>
            <Form error widths='equal' className={style.form} onSubmit={(e) => this.submit(e)}>
              <Form.Group>
                <Form.Input value={this.state.city} width={5} error={this.state.cityError} onChange={(e) => this.handleInput(e, 'city')} control={Input} inline name='city' label='Origin City' placeholder='Origin City'/>
                <Form.Input width={5} control={Select} error={this.state.peopleError} onChange={this.handleDropdown} name='people' label='How Many People?' options={options.optionsPeople} placeholder='Number of People' value={this.state.people} />
              </Form.Group>
              <Form.Group>
                <Form.Input control={Select} error={this.state.budgetError} onChange={this.handleDropdown} name='budget' label="What's the Budget?" options={options.optionsBudget} placeholder='Budget' value={this.state.budget} />
                <Form.Input control={Select} error={this.state.durationError} onChange={this.handleDropdown} name='duration' label="Travelling Duration?" options={options.optionsDuration} placeholder='Duration' value={this.state.duration} />
              </Form.Group>
              <Divider section className={style.divider} />
              <Form.Group>
                <Form.Input onClick={this.checkDates('startDate')} min={this.state.today} control={Input} value={this.state.startDate} error={this.state.startDateError} onChange={(e) => this.handleInput(e, 'startDate')} type='date' name='startDate' label='Start Date' placeholder='Start Date' />
                <Form.Input onClick={this.checkDates('stopDate')} min={this.state.today} control={Input} value={this.state.stopDate} error={this.state.stopDateError} onChange={(e) => this.handleInput(e, 'stopDate')} type='date' name='stopDate' label='Stop Date' placeholder='Stop Date' />                
              </Form.Group>
              <Divider section className={style.divider} />              
              <Form.Group>
                <Checkbox
                  className={style.checkbox}
                  radio
                  label='An Adventure'
                  name='preference'
                  value='adventure'
                  checked={this.state.preference === 'adventure'}
                  onChange={(e) => this.handleChange(e, 'adventure')}
                />
                <Checkbox
                  className={style.checkbox}
                  radio
                  label='A Bachelor Party'
                  name='preference'
                  value='bachelor'
                  checked={this.state.preference === 'bachelor'}
                  onChange={(e) => this.handleChange(e, 'bachelor')}
                />
                <Checkbox
                  className={style.checkbox}
                  radio
                  label='For Sightseeing'
                  name='preference'
                  value='sightseeing'
                  checked={this.state.preference === 'sightseeing'}
                  onChange={(e) => this.handleChange(e, 'sightseeing')}
                />
                <Checkbox
                  className={style.checkbox}
                  radio
                  label='For Sports'
                  name='preference'
                  value='sports'
                  checked={this.state.preference === 'sports'}
                  onChange={(e) => this.handleChange(e, 'sports')}
                />
                <Checkbox
                  className={style.checkbox}
                  radio
                  label='For Partying'
                  name='preference'
                  value='party'
                  checked={this.state.preference === 'party'}
                  onChange={(e) => this.handleChange(e, 'party')}
                />
                <Checkbox
                  className={style.checkbox}
                  radio
                  label='For Relaxing'
                  name='preference'
                  value='relaxing'
                  checked={this.state.preference === 'relaxing'}
                  onChange={(e) => this.handleChange(e, 'relaxing')}
                />
                <Checkbox
                  className={style.checkbox}
                  radio
                  label='For a Family'
                  name='preference'
                  value='family'
                  checked={this.state.preference === 'family'}
                  onChange={(e) => this.handleChange(e, 'family')}
                />
              </Form.Group>
              {this.state.showError && <Message
                compact
                error={this.state.showError}
                content={this.state.inputErrors[0].errorMessage}
              />}
              <Form.Group className={style.submit}>
                <Button className={style.button}>
                  Go To
                </Button>
              </Form.Group>
            </Form>
          </div>
        </Segment>    
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    coords: state.geolocation.coords
  }
}

export default connect(mapStateToProps)(Home)